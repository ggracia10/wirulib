﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class BaseData
{
    abstract public void SaveData();
    abstract public void LoadData();


}
