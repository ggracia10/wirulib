﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WiruLib
{
    public class Utils
    {
        public const float COS_30 = 0.86602540F;
        public const float COS_45 = 0.70710678f;
        public const float COS_60 = 0.5F;

        public const float SIN_30 = 0.5F;
        public const float SIN_45 = 0.70710678f;
        public const float SIN_60 = 0.86602540F;

        public const int LAYER_DEFAULT = 0;
        public const int LAYER_WATER = 4;
        public const int LAYER_GROUND = 8;
        public const int LAYER_OBSTACLES = 9;
        public const int LAYER_INTERACTUABLE = 10;

        //!Cast Int to String
        public static string Itos(int num, int cif)
        {
            string ret = "";
            for (int i = 0; i < cif; i++)
            {
                num = num % (int)Mathf.Pow(10, cif - i);

                if (num / Mathf.Pow(10, cif - i - 1) < 1)
                    ret += "0";
                else ret += ((int)(num / Mathf.Pow(10, cif - i - 1))).ToString();
            }
            return ret;
        }

        //! Cast String to Int
        public static int Stoi(string num)
        {
            int number = 0;
            for (int i = 0; i < num.Length; i++)
            {
                number += ((int)num[i] - 48) * (int)Mathf.Pow(10, num.Length - 1 - i);
            }
            return number;
        }

        public static float GetRectXValue(float initValueX, float finalValueX, float initValueY, float finalValueY, float currentYValue)
        {
            Vector2 dirVect = new Vector2(finalValueX - initValueX, finalValueY - initValueY);
            float a = dirVect.y;
            float b = -dirVect.x;
            float c = -a * initValueX - b * initValueY;

            return (-b * currentYValue - c) / a;
        }

        public static float GetRectYValue(float initValueX, float finalValueX, float initValueY, float finalValueY, float currentXValue)
        {
            Vector2 dirVect = new Vector2(finalValueX - initValueX, finalValueY - initValueY);
            float a = dirVect.y;
            float b = -dirVect.x;
            float c = -a * initValueX - b * initValueY;

            return (-a * currentXValue - c) / b;
        }

        //Para convertir la rotacion que va de 0 a 360 a una rotacion de -180 a 180
        public static float RealRotationToEditorRotation(float rotation)
        {
            return rotation - 360 * System.Convert.ToSByte(rotation > 180);
        }

        public static Vector3 RealRotationToEditorRotation(Vector3 rotation)
        {
            rotation.x = RealRotationToEditorRotation(rotation.x);
            rotation.y = RealRotationToEditorRotation(rotation.y);
            rotation.z = RealRotationToEditorRotation(rotation.z);

            return rotation;
        }

        public static float Get360Rotation(float rotation)
        {
            return rotation % 360;
        }

        public static Vector3 Get360Rotation(Vector3 rotation)
        {
            rotation.x = Get360Rotation(rotation.x);
            rotation.y = Get360Rotation(rotation.y);
            rotation.z = Get360Rotation(rotation.z);

            return rotation;
        }

        public static Vector3 ClampVector(Vector3 clamp, Vector3 min, Vector3 max)
        {
            clamp.x = Mathf.Clamp(clamp.x, min.x, max.x);
            clamp.y = Mathf.Clamp(clamp.y, min.y, max.y);
            clamp.z = Mathf.Clamp(clamp.z, min.z, max.z);
            return clamp;
        }

        public static T LoadFromJsonFile<T>(string path)
        {
            TextAsset file = Resources.Load(path) as TextAsset;
            if (file == null)
            {
                Debug.LogError("Error while Loading: " + path + " This file does not exists. Loading Tempalte...");
                return default(T);
            }
            string json = file.text;
            T temp = JsonUtility.FromJson<T>(json);
            Debug.Log("File loaded successfuly: " + path);
            return temp;
        }

        public static Color rgbToUnityColor(byte r, byte g, byte b)
        {
            return new Color((float)r / 255f, (float)g / 255f, (float)b / 255);
        }

        public static Color rgbaToUnityColor(byte r, byte g, byte b, byte a)
        {
            return new Color((float)r / 255f, (float)g / 255f, (float)b / 255, (float)a / 255f);
        }
    }
}