﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WiruLib {
    public class FPS : MonoBehaviour
    {
        bool enableFPS;
        CanvasGroup group;
        [SerializeField] TMPro.TextMeshProUGUI fpsText = null;
        [SerializeField] TMPro.TextMeshProUGUI avgText = null;
        [SerializeField] TMPro.TextMeshProUGUI minText = null;

        double totalCounts;
        double minFPS, maxFPS, avgFPS, totalFPS, currentFPS;
        double currentTime;

        // Start is called before the first frame update
        void Start()
        {
            DontDestroyOnLoad(transform.parent.gameObject);
            enableFPS = true;
            group = GetComponent<CanvasGroup>();
            minFPS = 60;
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.F12))
            {
                enableFPS = !enableFPS;
                group.alpha = System.Convert.ToInt16(enableFPS);
            }
            if(currentTime < 2)
            {
                currentTime += Time.deltaTime;
            }
            if (currentTime >= 2 && enableFPS)
            {
                currentFPS = (1f / Time.unscaledDeltaTime);
                
                totalFPS += currentFPS;
                avgFPS = totalFPS / ++totalCounts;
                if (currentFPS < minFPS) minFPS = currentFPS;

                SetText(fpsText, "FPS: ", currentFPS);
                SetText(avgText, "AVG: ", avgFPS);
                SetText(minText, "MIN: ", minFPS);
            }
        }
        void SetText(TMPro.TextMeshProUGUI _tmPro, string _title, double _fps)
        {
            _tmPro.text = _title + _fps.ToString("00.00");
            if (_fps >= 30) _tmPro.color = Color.white;
            else if (_fps >= 10) _tmPro.color = Color.yellow;
            else _tmPro.color = Color.red;

        }
    }
}
