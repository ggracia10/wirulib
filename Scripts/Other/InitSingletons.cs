﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WiruLib;

namespace WiruLib
{
    public class InitSingletons : MonoBehaviour
    {
        [SerializeField] GameObject rewiredPrefab = null;
        [SerializeField] GameObject fpsPrefab = null;
        [SerializeField] bool enableFPS;

        private void Awake()
        {
            if (GameObject.Find("Rewired Prefab") != null) { return; }

            Instantiate(rewiredPrefab).name = "Rewired Prefab";

            SceneManager.Instance.Init_SceneManager();
            GameManager.Instance.Init_GameManager();
            InputManager.Instance.Init_InputManager();
            Localize.WiruLocalization.Instance.Init_WiruLocalization();

#if UNITY_EDITOR || DEVELOPMENT_BUILD
            if (enableFPS)
            {
                Instantiate(fpsPrefab);
            }
#endif

        }
    }
}