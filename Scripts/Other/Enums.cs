﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enums
{
    public enum Platforms { Desktop, iOS, Android, PS4, XBOX_ONE, Switch};
    public enum Controllers { Keyboard, PS3, PS4, XBOX360, XBOX_ONE, Switch, Other};

    public enum RegionMode { Occidental, Asian };
}
