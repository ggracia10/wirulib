﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace WiruLib
{
    public class CameraFX : MonoBehaviour
    {
        public const float REGULAR_FADE_TIME = 1f;

        public const float REGULAR_ZOOM_SPEED = 10f;

        private Vector3 originalCameraPosition;

        [Header("Main Properties")]
        [SerializeField] private Camera originalCamera;
        [SerializeField] private Canvas mainCanvas;

        GameObject[] fadeImage;

        float currentZoom, originalZoom;

        bool fading, zooming, shaking;
        public bool Fading
        {
            get { return fading; }
        }

        public bool Zooming
        {
            get { return zooming; }
        }

        public bool Shaking
        {
            get { return shaking; }
        }

        private void Awake()
        {
            fadeImage = GameObject.FindGameObjectsWithTag("FadeImage");
            if (originalCamera == null)
            {
                if (GetComponent<Camera>()) originalCamera = GetComponent<Camera>();
                else originalCamera = Camera.main;
            }
            if(mainCanvas == null)
            {
                mainCanvas = GameObject.Find("MainCanvas").GetComponent<Canvas>();
            }
            originalZoom = originalCamera.fieldOfView;
            originalCamera.layerCullSpherical = true;
            CameraFadeIn(Color.black, REGULAR_FADE_TIME);
        }

        public void CameraFadeIn(Color fadeColor, float fadeTime = REGULAR_FADE_TIME)
        {
            if (fading) return;
            StartCoroutine(CameraFadeInCor(fadeColor, fadeTime));
        }

        IEnumerator CameraFadeInCor(Color fadeColor, float fadeTime)
        {
            fading = true;
            fadeColor.a = 1;
            for (float t = fadeTime; t > 0; t -= Time.deltaTime)
            {
                fadeColor.a = t / fadeTime;

                for (int i = 0; i < fadeImage.Length; i++)
                {
                    fadeImage[i].GetComponent<Image>().color = fadeColor;
                }
                yield return null;
            }

            fadeColor.a = 0;
            for (int i = 0; i < fadeImage.Length; i++)
            {
                fadeImage[i].GetComponent<Image>().color = fadeColor;
            }
            yield return null;
            fading = false;
        }

        public void CameraFadeOut(Color fadeColor, float fadeTime = REGULAR_FADE_TIME)
        {
            if (fading) return;
            StartCoroutine(CameraFadeOutCor(fadeColor, fadeTime));
        }

        IEnumerator CameraFadeOutCor(Color fadeColor, float fadeTime)
        {
            fading = true;
            fadeColor.a = 0;
            for (float t = 0; t < fadeTime; t += Time.deltaTime)
            {
                fadeColor.a = t / fadeTime;

                for (int i = 0; i < fadeImage.Length; i++)
                {
                    fadeImage[i].GetComponent<Image>().color = fadeColor;
                }
                yield return null;
            }

            fadeColor.a = 1;
            for (int i = 0; i < fadeImage.Length; i++)
            {
                fadeImage[i].GetComponent<Image>().color = fadeColor;
            }
            yield return null;
            fading = false;
        }

        public void ZoomIn(float quant, float zoomSpeed)
        {
            StartCoroutine(Zoom(currentZoom / quant, zoomSpeed));
        }

        public void ZoomOut(float quant, float zoomSpeed)
        {
            StartCoroutine(Zoom(currentZoom * quant, zoomSpeed));
        }

        public void NeutralZoom(float zoomSpeed)
        {
            StartCoroutine(Zoom(originalZoom, zoomSpeed));
        }

        IEnumerator Zoom(float zoomToGo, float zoomSpeed)
        {
            zooming = true;
            while (currentZoom < zoomToGo && currentZoom >= zoomToGo ||
                 currentZoom > zoomToGo && currentZoom <= zoomToGo)
            {
                currentZoom += (Mathf.Sign(zoomToGo - currentZoom) * Time.deltaTime * zoomSpeed);
                yield return null;
            }
            currentZoom = zoomToGo;
            zooming = false;
            yield return null;
        }

        public void CameraShake(float xOffset, float yOffset, float time, Camera cam = null, bool acceleration = false)
        {
            Camera temp = cam;
            if (temp == null)
            {
                temp = originalCamera;
            }

            StartCoroutine(CameraShakeCor(Mathf.Abs(xOffset), Mathf.Abs(yOffset), Mathf.Abs(time), temp, acceleration));
        }

        IEnumerator CameraShakeCor(float xOffset, float yOffset, float time, Camera cam, bool acceleration)
        {
            if (cam == null)
            {
                Debug.LogError("No camera detected. Please be sure your Camera is tagged as Main Camera.");
            }
            else
            {
                shaking = true;
                originalCameraPosition = Vector3.zero;
                Vector3 randomOffset = Vector3.zero;

                for (float t = 0; t < time; t += Time.deltaTime)
                {
                    originalCameraPosition = cam.transform.position - randomOffset;
                    randomOffset.x = Random.Range(-xOffset, xOffset);
                    randomOffset.y = Random.Range(-yOffset, yOffset);

                    cam.transform.position = originalCameraPosition + randomOffset;

                    yield return null;
                }

                cam.transform.position = originalCameraPosition;
            }
            shaking = false;
            yield return null;
        }
    }
}
