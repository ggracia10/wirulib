﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WiruLib {
    public class ThirdPerson3DCamera : MonoBehaviour
    {
        const float MAX_AUTO_Y = 2;
        float MAX_HEIGHT_CAMERA;

        [Header("Camera Speed Properties")]
        [SerializeField] float cameraMovementAccel = 0;
        [SerializeField] float cameraRotationAccel = 0;
        [SerializeField] float cameraDistanceAccel = 0;
        [SerializeField] float rotationSpeed = 0;
        [SerializeField] float distanceSpeed = 0;

        [Header("Camera Distance Properties")]
        [SerializeField] float cameraMaxDistance = 0;
        [SerializeField] float cameraMinDistance = 0;
        [SerializeField] float cameraCurrentDistance = 0;

        [Header("Camera AI")]
        [SerializeField] Vector3[] targetVisiblePoints = null;
        [SerializeField] Vector3[] backPoints = null;
        [SerializeField] Vector3[] roofPoints = null;

        [SerializeField] GameObject target = null;

        [Header("Camera Max Angles")]
        [SerializeField] Vector2 maxAngle = Vector2.zero;
        [SerializeField] Vector2 minAngle = Vector2.zero;

        [SerializeField] UnityEngine.PostProcessing.PostProcessingBehaviour postProcessing;
        UnityEngine.PostProcessing.DepthOfFieldModel.Settings depth;
        
        Vector3 cameraCurrentPosition, cameraPositionToGo;
        public Vector3 cameraCurrentRotation, cameraRotationToGo;
        float distanceToGo = 0;

        Vector3 inputRotation, lastInputRotation;
        Vector3 targetDistance;
        Vector3 lastPosition, currentVelocity;
        
        RaycastHit hit;
        public int invisibilePointsCount;

        public bool isTargetVisible;
        
        Transform cameraTrans;
        SphereCollider cameraCol;

        //Tempral variables:
        float tempAngle;
        Vector2 tempVect1, tempVect2;
        public int collidingCount;

        LayerMask layerMask;

        public Vector3 InputRotation
        {
            get { return inputRotation; }
            set { inputRotation = value; }
        }

        // Start is called before the first frame update
        void Start()
        {
            layerMask = 0b011111111;
            cameraCurrentPosition = transform.position;
            cameraCurrentRotation = cameraRotationToGo = new Vector3(25,0,0);
            cameraTrans = transform.GetChild(0);
            cameraCol = cameraTrans.GetComponent<SphereCollider>();

            MAX_HEIGHT_CAMERA = cameraMaxDistance * Mathf.Sin(Mathf.Deg2Rad * maxAngle.x);
            depth = postProcessing.profile.depthOfField.settings;
        }

        void LateUpdate()
        {
            UpdateTargetVisible();
            CheckIA();
        }

        private void FixedUpdate()
        {
            currentVelocity = cameraTrans.position - lastPosition;
            lastPosition = cameraTrans.position;

            UpdateRotation();
            UpdatePosition();
            UpdateDistance();
        }

        void UpdateTargetVisible()
        {
            isTargetVisible = true;

            invisibilePointsCount = 0;
            //Si todos los rayos que se tiran al target son cortados, es que la cámara no ve al jugador
            for (int i = 0; i < targetVisiblePoints.Length; i++)
            {
                targetDistance = (target.transform.position + targetVisiblePoints[i] - cameraTrans.position);

                if (Physics.Raycast(cameraTrans.position, targetDistance.normalized, out hit, targetDistance.magnitude, layerMask))
                {
                    if (hit.collider.gameObject.tag != "Player" && !hit.collider.isTrigger)
                    {
                        invisibilePointsCount++;
                    }
                }
            }

            isTargetVisible = (invisibilePointsCount < targetVisiblePoints.Length);
        }

        void UpdatePosition()
        {
            cameraPositionToGo = target.transform.position;
            cameraCurrentPosition = Vector3.Lerp(cameraCurrentPosition, cameraPositionToGo, cameraMovementAccel * Time.deltaTime);
            transform.position = cameraCurrentPosition;
        }

        bool CheckBackCollision()
        {
            invisibilePointsCount = 0;
            //Si algun rayo hacia la espalda de la cámara toca algo, es que está fuera de la distancia de seguridad
            for(int i = 0; i < backPoints.Length; i++)
            {
                if(Physics.Raycast(cameraTrans.position, transform.TransformDirection(backPoints[i]), cameraCol.radius + backPoints[i].magnitude))
                {
                    invisibilePointsCount++;
                    break;
                }
            }
            return invisibilePointsCount > 0;
        }

        void UpdateDistance()
        {
            if(collidingCount > 0) //Si la cámara está chocando reducir la distancia con el jugador
            {
                distanceToGo = Mathf.Clamp(cameraCurrentDistance - 60/cameraDistanceAccel * Mathf.Max(currentVelocity.magnitude, 0.1f), cameraMinDistance, cameraMaxDistance);
            }
            else if (!CheckBackCollision()) //Si no está chocando y no está en la distancia de seguridad, aumentar distancia con jugador hasta máxima
            {
                distanceToGo = Mathf.Clamp(cameraCurrentDistance + 60f/cameraDistanceAccel * currentVelocity.magnitude, cameraMinDistance, cameraMaxDistance);
            }

            cameraCurrentDistance = Mathf.Lerp(cameraCurrentDistance, distanceToGo, cameraDistanceAccel * Time.deltaTime);

            cameraTrans.localPosition = Vector3.back * cameraCurrentDistance;
            UpdateDepthOfField();
        }

        void UpdateDepthOfField(bool _reset = false)
        {
            if (_reset)
            {
                depth.focusDistance = cameraMaxDistance;
            }
            else
            {
                depth.focusDistance = cameraCurrentDistance;
            }
            postProcessing.profile.depthOfField.settings = depth;
        }

        bool DetectTargetRoof()
        {
            int collisions = 0;
            //Tirar raycast hacia los puntos de deteccion, si uno golpea con algo, es que hay techo
            for(int i = 0; i < roofPoints.Length; i++)
            {
                if (Physics.Raycast(target.transform.position, target.transform.TransformDirection(roofPoints[i]), target.transform.position.y + MAX_HEIGHT_CAMERA))
                {
                    collisions++;
                    break;
                }
            }
            return collisions > 0;
        }

        void CheckIA()
        {
            tempVect1.x = target.transform.forward.x;
            tempVect1.y = target.transform.forward.z;
            tempVect2.x = transform.forward.x;
            tempVect2.y = transform.forward.z;


            if (!isTargetVisible && inputRotation.magnitude == 0) //Si el jugador no se ve y el jugador no está dando input a cámara
            {
                //Por defecto sube la cámara
                inputRotation.x = -Mathf.Clamp((Utils.Get360Rotation(target.transform.rotation.eulerAngles.x) - Utils.Get360Rotation(transform.rotation.eulerAngles.x)), -0.5f, 0.5f);
                
                //Si detecto techo, baja la cámara
                if (DetectTargetRoof()) 
                    inputRotation.x = -inputRotation.x;
                
                //Para que no baje a menos altura que estar alineado con el jugador (camara paralela al suelo)
                if(cameraTrans.position.y <= target.transform.position.y + MAX_AUTO_Y && inputRotation.x < 0)
                {
                    inputRotation.x = 0; 
                }
                lastInputRotation.x = inputRotation.x;

                if(lastInputRotation.y == 0) //Si el anterior input horizontal fue 0
                {
                    //Angulo entre camara y jugador
                    tempAngle = Utils.Get360Rotation((target.transform.rotation.eulerAngles.y) - (transform.rotation.eulerAngles.y)); 

                    lastInputRotation.y = Mathf.Sign(tempAngle); //Asignar trayectoría
                }
                else
                {
                    inputRotation.y = lastInputRotation.y; //Seguir la trayectoría que llevaba de antes)
                }

                inputRotation.z = 0;
                inputRotation.Normalize();
            }
            else if (isTargetVisible)
            {
                lastInputRotation.x = 0;
                lastInputRotation.y = 0;
            }
        }

        void UpdateRotation()
        {
            cameraRotationToGo = Utils.ClampVector(cameraRotationToGo + inputRotation * rotationSpeed * Time.deltaTime,minAngle, maxAngle);
            cameraCurrentRotation = Utils.ClampVector(Vector3.Lerp(cameraCurrentRotation, cameraRotationToGo, cameraRotationAccel * Time.deltaTime), minAngle, maxAngle);
            
            transform.rotation = Quaternion.Euler(cameraCurrentRotation);
        }

        private void OnDrawGizmos()
        {
            if (target != null || targetVisiblePoints != null)
            {
                Gizmos.color = Color.red;
                for (int i = 0; i < targetVisiblePoints.Length; i++)
                {
                    Gizmos.DrawSphere(target.transform.position + targetVisiblePoints[i], 0.1f);
                }
            }
            if (backPoints != null)
            {
                Gizmos.color = Color.blue;
                for (int i = 0; i < backPoints.Length; i++)
                {
                    Gizmos.DrawSphere(transform.GetChild(0).position + transform.TransformDirection(backPoints[i]), 0.1f);
                }
            }

            if(roofPoints != null)
            {
                Gizmos.color = Color.green;
                for(int i = 0; i < roofPoints.Length; i++)
                {
                    Gizmos.DrawSphere(target.transform.position + target.transform.TransformDirection(roofPoints[i]), 0.1f);
                }
            }
        }

        private void OnTriggerEnter(Collider collision)
        {
            if (collision.gameObject.tag != "Player" && !collision.isTrigger)
                collidingCount++;
        }

        private void OnTriggerExit(Collider collision)
        {
            if(collision.gameObject.tag != "Player" && !collision.isTrigger)
                collidingCount--;
        }

        private void OnDestroy()
        {
            UpdateDepthOfField(true);
        }
    }
}
