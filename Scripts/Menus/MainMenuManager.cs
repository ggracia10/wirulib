using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WiruLib;

public class MainMenuManager : MenuManager
{

    [SerializeField] protected string[] scenesToGo;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        base.Update();
    }

    public void ChangeScene(int index) {
        SceneManager.Instance.ChangeSceneAsync(scenesToGo[index]);
    }

    public void CloseGame() {
        GameManager.Instance.CloseGame();
    }
}
