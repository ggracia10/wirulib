﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WiruLib;

public class ActivatePause : MonoBehaviour
{
    [Header("Pause Canvas Groups")]
    [SerializeField] CanvasGroup generalPauseGroup;
    [SerializeField] CanvasGroup pauseMenuGroup, pauseTitleGroup;

    public bool inTransition;

    // Start is called before the first frame update
    void Awake()
    {
        generalPauseGroup.alpha = 0;
        generalPauseGroup.interactable = false;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void ActivePause(bool value)
    {
        pauseMenuGroup.interactable = false;
        pauseTitleGroup.interactable = false;

        if (value)
        {
            Time.timeScale = 0;

            StartCoroutine(ActivatePauseCor());
        }
        else
        {
            Time.timeScale = 0;
            StartCoroutine(DeactivatePauseCor());
        }
    }

    IEnumerator ActivatePauseCor(float time = 1f)
    {
        inTransition = true;

        for (float currentTime = 0; currentTime <= time; currentTime += Time.unscaledDeltaTime)
        {
            generalPauseGroup.alpha = currentTime / time;
            Debug.Log("HEEY " + currentTime + " / " + time);
            yield return null;
        }

        inTransition = false;
        yield return null;
    }

    IEnumerator DeactivatePauseCor(float time = 1f)
    {
        inTransition = true;
        for (float currentTime = time; currentTime > 0; currentTime -= Time.unscaledDeltaTime)
        {
            generalPauseGroup.alpha = currentTime / time;
            yield return null;
        }
        Time.timeScale = 1;
        inTransition = false;
        yield return null;
    }
}
