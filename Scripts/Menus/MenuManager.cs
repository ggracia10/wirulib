﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace WiruLib {
    public class MenuManager : MonoBehaviour
    {
        [Header("MenuManager Base Properties")]
        [SerializeField] protected GameObject firstButtonSelected;
        [SerializeField] protected GameObject[] buttons;

        [Header ("Test Things MenuManager Base")]
        [SerializeField] protected GameObject currentButtonSelected;
        [SerializeField] protected GameObject eventSystemCurrentSelected;

        protected bool changingSelected;

        // Start is called before the first frame update
        protected virtual void Awake()
        {
            
        }

        protected virtual void Start()
        {
            if (firstButtonSelected != null)
            {
                StartCoroutine(ChangeSelectedButton(firstButtonSelected));
            }
        }

        // Update is called once per frame
        protected virtual void Update()
        {
            UpdateControls();
            //CheckSelected();
        }

        protected virtual void LateUpdate()
        {
            CheckSelected();
        }

        protected virtual void CheckSelected()
        {
            eventSystemCurrentSelected = EventSystem.current.currentSelectedGameObject;
            if(EventSystem.current.currentSelectedGameObject == null && !changingSelected)
            {
                //EventSystem.current.SetSelectedGameObject(currentButtonSelected);
                StartCoroutine(ChangeSelectedButton(currentButtonSelected));
            }
            else if (EventSystem.current.currentSelectedGameObject != currentButtonSelected && EventSystem.current.currentSelectedGameObject != null)
            {
                currentButtonSelected = EventSystem.current.currentSelectedGameObject;
                
            }
        }

        protected virtual void UpdateControls()
        {

        }

        IEnumerator ChangeSelectedButton(GameObject btn)
        {
            changingSelected = true;
            yield return null;
            EventSystem.current.SetSelectedGameObject(null);
            
            EventSystem.current.SetSelectedGameObject(btn);
            Debug.Log("Change");
            btn.GetComponent<Button>().OnSelect(null);
            changingSelected = false;
        }
    }
}
