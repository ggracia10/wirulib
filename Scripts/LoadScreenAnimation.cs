﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using WiruLib;

public class LoadScreenAnimation : MonoBehaviour
{
    [SerializeField] CanvasGroup fadeGroup = null;
    [SerializeField] Image background = null;
    [SerializeField] Image circle = null;
    [SerializeField] Color[] screenColors = null;
    

    Vector3 currentRotation;
    float rotationSpeed = 180;

    private void Awake()
    {
        background.color = screenColors[(int)SceneManager.Instance.loadingScreenColor];
        for(int i = 0; i < fadeGroup.transform.childCount; i++)
        {
            if (SceneManager.Instance.loadingScreenColor == SceneManager.LoadingScreenColor.Clear)
            {
                fadeGroup.transform.GetChild(i).GetComponent<Image>().color = screenColors[(int)SceneManager.LoadingScreenColor.Dark];
            }
            else
            {
                fadeGroup.transform.GetChild(i).GetComponent<Image>().color = screenColors[(int)SceneManager.LoadingScreenColor.Clear];
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        currentRotation -= Vector3.forward * Time.deltaTime * rotationSpeed;
        if (currentRotation.z <= -360) currentRotation.z += 360;

        circle.rectTransform.rotation = Quaternion.Euler(currentRotation);
    }
}
