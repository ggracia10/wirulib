﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
//using Rewired.Platforms.PS4;

namespace WiruLib
{
    public class InputManager : Singleton<InputManager>
    {
        public const float AXIS_DEAD_ZONE = 0.15f;

        List<Rewired.Player> rPlayer;
        Controller mainController;

        bool joystickConnected;
        Enums.Controllers currentController, lastController;
        List<Enums.Controllers> controllersConnected;

        public Enums.Controllers CurrentController
        {
            get { return currentController; }
        }
        
        void Awake()
        {
            rPlayer = new List<Rewired.Player>();
            // Subscribe to events
            controllersConnected = new List<Enums.Controllers>();
            ReInput.ControllerConnectedEvent += OnControllerConnected;
            ReInput.ControllerDisconnectedEvent += OnControllerDisconnected;
            ReInput.ControllerPreDisconnectEvent += OnControllerPreDisconnect;
        }

        // This function will be called when a controller is connected
        // You can get information about the controller that was connected via the args parameter
        void OnControllerConnected(ControllerStatusChangedEventArgs args)
        {
            Debug.Log("A controller was connected! Name = " + args.name + " Id = " + args.controllerId + " Type = " + args.controllerType);
            /*if(args.controllerType == ControllerType.Joystick)
            {
                joystickConnected = true;
                if(args.name.Contains("DualShock 4"))
                {
                    controllersConnected.Add(Enums.Controllers.PS4);
                    Debug.Log("PS4 controller Connected");
                }
                else if(args.name.Contains("DualShock 3"))
                {
                    controllersConnected.Add(Enums.Controllers.PS3);
                    Debug.Log("PS3 controller Connected");
                }
                else if(args.name.Contains("Xbox 360"))
                {
                    controllersConnected.Add(Enums.Controllers.XBOX360);
                    Debug.Log("X-Box 360 controller Connected");
                }
                else if (args.name.Contains("Xbox One"))
                {
                    controllersConnected.Add(Enums.Controllers.XBOX_ONE);
                    Debug.Log("X-Box One controller Connected");
                }
                else if(args.name.Contains("Nintendo Switch"))
                {
                    controllersConnected.Add(Enums.Controllers.Switch);
                    Debug.Log("Nintendo Switch controller Connected");
                }
                else
                {
                    controllersConnected.Add(Enums.Controllers.Other);
                    Debug.Log("Unknown controller Connected");
                }
            }*/
        }

        // This function will be called when a controller is fully disconnected
        // You can get information about the controller that was disconnected via the args parameter
        void OnControllerDisconnected(ControllerStatusChangedEventArgs args)
        {
            Debug.Log("A controller was disconnected! Name = " + args.name + " Id = " + args.controllerId + " Type = " + args.controllerType);
        }

        // This function will be called when a controller is about to be disconnected
        // You can get information about the controller that is being disconnected via the args parameter
        // You can use this event to save the controller's maps before it's disconnected
        void OnControllerPreDisconnect(ControllerStatusChangedEventArgs args)
        {
            Debug.Log("A controller is being disconnected! Name = " + args.name + " Id = " + args.controllerId + " Type = " + args.controllerType);
        }

        void OnDestroy()
        {
            // Unsubscribe from events
            ReInput.ControllerConnectedEvent -= OnControllerConnected;
            ReInput.ControllerDisconnectedEvent -= OnControllerDisconnected;
            ReInput.ControllerPreDisconnectEvent -= OnControllerPreDisconnect;
        }

        /*************************************************************************************************/
        /**
         * @brief Inicializador del InputManager
         */
        // Use this for initialization
        public void Init_InputManager()
        {
            rPlayer.Add(ReInput.players.GetPlayer(0));
            mainController = rPlayer[0].controllers.GetLastActiveController();
            lastController = currentController = Enums.Controllers.Keyboard;
            //SetUpPS4Controllers();
        }

        // public void SetUpPS4Controllers()
        // {
        //     PS4GamepadExtension ext;
        //     for (int i = 0; i < rPlayer.Count; i++)
        //     {
        //         for(int j = 0; j < rPlayer[i].controllers.Joysticks.Count; j++)
        //         {
        //             if (rPlayer[i].controllers.Joysticks[j].name.Contains("DualShock 4"))
        //             {
        //                 ext = rPlayer[i].controllers.Joysticks[j].GetExtension<PS4GamepadExtension>();
        //                 if (ext != null)
        //                 {
        //                     ext.SetLightColor(Color.red);
        //                 }
        //             }
        //         }
        //     }
        // }

        private void LateUpdate()
        {
            currentController = GetCurrentController();
            //if (Cursor.visible) Cursor.visible = false;
            if (currentController != lastController)
            {
                lastController = currentController;
                if(currentController == Enums.Controllers.Keyboard)
                {
                    Cursor.lockState = CursorLockMode.Confined;
                }
                else
                {
                    Cursor.lockState = CursorLockMode.Locked;
                }
                Debug.LogWarning("Last Controller used changed to: " + currentController);
            }
        }

        Enums.Controllers GetCurrentController()
        {
            Enums.Controllers temp = currentController;
            Controller controller = rPlayer[0].controllers.GetLastActiveController();
            if(controller == null)
            {
                if(rPlayer[0].controllers.joystickCount > 0)
                {
                    controller = rPlayer[0].controllers.Joysticks[0];
                }
                else
                {
                    controller = rPlayer[0].controllers.Keyboard;
                }
                
            }
            if (controller.type == ControllerType.Joystick)
            {
                joystickConnected = true;
                if (controller.name.Contains("DualShock 4"))
                {
                    temp = Enums.Controllers.PS4;
                }
                else if (controller.name.Contains("DualShock 3"))
                {
                    temp = Enums.Controllers.PS3;
                }
                else if (controller.name.Contains("Xbox 360"))
                {
                    temp = Enums.Controllers.XBOX360;
                }
                else if (controller.name.Contains("Xbox One"))
                {
                    temp = Enums.Controllers.XBOX_ONE;
                }
                else if (controller.name.Contains("Nintendo Switch"))
                {
                    temp = Enums.Controllers.Switch;
                }
                else
                {
                    temp = Enums.Controllers.Other;
                }
            }
            else if(controller.type == ControllerType.Keyboard || controller.type == ControllerType.Mouse)
            {
                temp = Enums.Controllers.Keyboard;
            }
            return temp;
        }

        public void AddPlayer(int id)
        {
            if (id < rPlayer.Count && id >= 0)
            {
                rPlayer[id] = ReInput.players.GetPlayer(id);
            }
            else
            {
                rPlayer.Add(ReInput.players.GetPlayer(id));
            }
        }

        public void RemovePlayer(int id)
        {
            rPlayer[id] = null;
        }

        /*************************************************************************************************/
        /**
         * @brief Devuelve el valor del axis pasado por parametro
         * 
         * @param string axis El nombre del axis que queremos saber el valor actual
         * 
         * @returns Valor del axis
         */
        public float GetAxis(string axis, int player = 0)
        {
            if (player > rPlayer.Count || player < 0)
            {
                Debug.LogError(" Player: [" + player + "] is out of range. Length of Player list is " + rPlayer.Count + ".");
                return 0;
            }
            else if (rPlayer[player] == null)
            {
                Debug.LogError("Player: [" + player + "] is not connected.");
                return 0;
            }
            
            return Mathf.Clamp(rPlayer[player].GetAxis(axis),-1,1);
        }
        /*************************************************************************************************/
        /**
         * @brief Devuelve si cualquier botón ha sido accionado en este frame
         * 
         * @returns True si justo en este frame algún botón ha sido presionado
         */
        public bool GetAnyButtonDown(int player = 0)
        {
            if(player > rPlayer.Count || player < 0)
            {
                Debug.LogError(" Player: [" + player + "] is out of range. Length of Player list is " + rPlayer.Count + ".");
                return false;
            }
            else if(rPlayer[player] == null)
            {
                Debug.LogError("Player: [" + player + "] is not connected.");
                return false;
            }
            return rPlayer[player].GetAnyButtonDown();
        }

        /*************************************************************************************************/
        /**
         * @brief Devuelve si el botón pasado por parámetro ha sido accionado en este frame
         * 
         * @param string btn El nombre del botón
         * 
         * @returns True si es justo este frame
         */
        public bool GetButtonDown(string btn, int player = 0)
        {
            if (player > rPlayer.Count || player < 0)
            {
                Debug.LogError(" Player: [" + player + "] is out of range. Length of Player list is " + rPlayer.Count + ".");
                return false;
            }
            else if (rPlayer[player] == null)
            {
                Debug.LogError("Player: [" + player + "] is not connected.");
                return false;
            }
            return rPlayer[player].GetButtonDown(btn);
        }

        /*************************************************************************************************/
        /**
         * @brief Devuelve si el botón pasado por parámetro ha sido levantado en este frame
         * 
         * @param string btn El nombre del botón
         * 
         * @returns True si es justo este frame
         */
        public bool GetButtonUp(string btn, int player = 0)
        {
            if (player > rPlayer.Count || player < 0)
            {
                Debug.LogError(" Player: [" + player + "] is out of range. Length of Player list is " + rPlayer.Count + ".");
                return false;
            }
            else if (rPlayer[player] == null)
            {
                Debug.LogError("Player: [" + player + "] is not connected.");
                return false;
            }
            return rPlayer[player].GetButtonUp(btn);
        }

        /*************************************************************************************************/
        /**
         * @brief Devuelve si el botón pasado por parámetro está siendo presionado en este frame
         * 
         * @param string btn El nombre del botón
         * 
         * @returns True si está siendo presionado
         */
        public bool GetButton(string btn, int player = 0)
        {
            if (player > rPlayer.Count || player < 0)
            {
                Debug.LogError(" Player: [" + player + "] is out of range. Length of Player list is " + rPlayer.Count + ".");
                return false;
            }
            else if (rPlayer[player] == null)
            {
                Debug.LogError("Player: [" + player + "] is not connected.");
                return false;
            }
            return rPlayer[player].GetButton(btn);
        }
    }
}
