﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

namespace WiruLib
{
    public class SceneManager : Singleton<SceneManager>
    {
        public enum LoadingScreenColor { Clear, Dark};
        public LoadingScreenColor loadingScreenColor;

        public string sceneToLoad = "";
        CameraFX camFx;

        public void Init_SceneManager()
        {

        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void ChangeScene(string sceneName, LoadingScreenColor color = LoadingScreenColor.Dark)
        {
            ChangeSceneAsync(sceneName, color);
        }

        public void ChangeSceneAsync(string sceneName, LoadingScreenColor color = LoadingScreenColor.Dark)
        {
            loadingScreenColor = color;
            camFx = Camera.main.GetComponent<CameraFX>();
            camFx.CameraFadeOut(Color.black);

            StartCoroutine(LoadAsyncScene(sceneName));
        }

        public string GetLocalizedString(string key)
        {
            return key;
        }

        IEnumerator LoadAsyncScene(string sceneToGo)
        {
            // The Application loads the Scene in the background as the current Scene runs.
            // This is particularly good for creating loading screens.
            // You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
            // a sceneBuildIndex of 1 as shown in Build Settings.
            while(camFx.Fading)
            {
                yield return null;
            }
            string loadingScreen = "CustomLoadingScreen";

            if(!UnityEngine.SceneManagement.SceneManager.GetSceneByName(loadingScreen).IsValid()) {
                loadingScreen = "WiruLibLoadScene";
            }

            AsyncOperation asyncLoad = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(loadingScreen);
            Debug.Log("Changing scene to: " + loadingScreen);

            float t = 0;
            // Wait until the asynchronous scene fully loads
            while (!asyncLoad.isDone || t < 1)
            {
                t += Time.unscaledDeltaTime;
                yield return null;
            }
            Debug.Log("Changing to: " + sceneToGo);
            asyncLoad = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(sceneToGo);
            while (!asyncLoad.isDone)
            {
               
                yield return null;
            }

        }

    }
}
