﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WiruLib
{
    public class BaseGameManager<T> : Singleton<T> where  T : BaseGameManager<T>
    {
        private Enums.RegionMode regionMode;
        protected Enums.Platforms currentPlatform;
        public bool gameIsFocus;

        // Start is called before the first frame update
        protected virtual void Awake()
        {
            currentPlatform = Enums.Platforms.Desktop;

#if UNITY_SWITCH
            currentPlatform = Enums.Platforms.Switch;
#elif UNITY_IOS
            currentPlatform = Enums.Platforms.iOS;
#elif UNITY_ANDROID
            currentPlatform = Enums.Platforms.Android;
#elif UNITY_PS4
            currentPlatform = Enums.Platforms.PS4;
#elif UNITY_XBOXONE
            currentPlatform = Enums.Platforms.XBOX_ONE;
#endif
        }

        public virtual void Init_GameManager()
        {

        }

        public Enums.RegionMode GetRegionMode()
        {
            return regionMode;
        }

        public bool IsAsianMode()
        {
            return regionMode == Enums.RegionMode.Asian;
        }

        public Enums.Platforms CurrentPlatform
        {
            get { return currentPlatform; }
        }

        private void OnApplicationFocus(bool focus)
        {
            gameIsFocus = focus;   
        }

        public void CloseGame() {
            Application.Quit();
        }
    }
}
